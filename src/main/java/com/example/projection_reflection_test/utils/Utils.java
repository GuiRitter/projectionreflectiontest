package com.example.projection_reflection_test.utils;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.example.projection_reflection_test.config.ApplicationContextHolder;
import com.example.projection_reflection_test.service.EntityConverter;

import org.springframework.hateoas.Link;
import org.springframework.util.Assert;

public class Utils {

	public static final SimpleDateFormat gregorianFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

	public static final BigDecimal ONE_HUNDRED;

	/**
	 * Precisão 10, escala 5.
	 */
	public static final BigDecimal VALOR_MAXIMO_10_5;

	/**
	 * Precisão 17, escala 5.
	 */
	public static final BigDecimal VALOR_MAXIMO_17_5;

	public static final SimpleDateFormat uniqueNameFormat = new SimpleDateFormat("mmss");

	static {
		ONE_HUNDRED = new BigDecimal("100");

		VALOR_MAXIMO_10_5 = new BigDecimal("99999.99999");
		Assert.isTrue(VALOR_MAXIMO_10_5.precision() == 10, "");
		Assert.isTrue(VALOR_MAXIMO_10_5.scale() == 5, "");

		VALOR_MAXIMO_17_5 = new BigDecimal("999999999999.99999");
		Assert.isTrue(VALOR_MAXIMO_17_5.precision() == 17, "");
		Assert.isTrue(VALOR_MAXIMO_17_5.scale() == 5, "");
	}

	public static String encodeURIComponent(String uri) {
		try {
			return URLEncoder.encode(uri, "UTF-8").replace("+", "%20");
		} catch (UnsupportedEncodingException ex) {
			return "";
		}
	}

	public static String decodeURIComponent(String uri) {
		try {
			return URLDecoder.decode(uri.replace("+", "%2B"), "UTF-8").replace("%2B", "+");
		} catch (UnsupportedEncodingException ex) {
			return "";
		}
	}
	
	public static <EntityType> EntityType getEntityFromURI(String URI, Class<EntityType> entityClass) {
		return ApplicationContextHolder.getContext().getBean(EntityConverter.class).convert(new Link(URI.replace("{?projection}", "")), entityClass);
	}

	public static <T> List<T> iteratorToList(final Iterable<T> iterable) {
		return StreamSupport.stream(iterable.spliterator(), false).collect(Collectors.toList());
	}
}

package com.example.projection_reflection_test.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.projection_reflection_test.model.Animal;
import com.example.projection_reflection_test.model.projection.AnimalProjection;

@RepositoryRestResource(collectionResourceRel = "animal", path = "animals", excerptProjection=AnimalProjection.class, exported=false)
public interface AnimalRepository  extends PagingAndSortingRepository<Animal, Long> {}

package com.example.projection_reflection_test.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.projection_reflection_test.model.Flower;
import com.example.projection_reflection_test.model.projection.FlowerProjection;

@RepositoryRestResource(collectionResourceRel = "flower", path = "flowers", excerptProjection=FlowerProjection.class, exported=false)
public interface FlowerRepository  extends PagingAndSortingRepository<Flower, Long> {}

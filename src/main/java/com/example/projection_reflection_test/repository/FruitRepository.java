package com.example.projection_reflection_test.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.projection_reflection_test.model.Fruit;
import com.example.projection_reflection_test.model.projection.FruitProjection;

@RepositoryRestResource(collectionResourceRel = "fruit", path = "fruits", excerptProjection=FruitProjection.class, exported=false)
public interface FruitRepository extends PagingAndSortingRepository<Fruit, Long> {}

package com.example.projection_reflection_test.model.projection;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface RegisterProjection {

	/*
	 * Propriedades da Entidade
	 */

	@JsonIgnore
	Long getId();
}

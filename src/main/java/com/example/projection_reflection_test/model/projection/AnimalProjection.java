package com.example.projection_reflection_test.model.projection;

import org.springframework.data.rest.core.config.Projection;

import com.example.projection_reflection_test.model.Animal;

@Projection(name = "animalProjection", types = { Animal.class })
public interface AnimalProjection extends RegisterProjection {

	String getName();
}

package com.example.projection_reflection_test.model.projection;

import org.springframework.data.rest.core.config.Projection;

import com.example.projection_reflection_test.model.Flower;

@Projection(name = "flowerProjection", types = { Flower.class })
public interface FlowerProjection extends RegisterProjection {

	String getName();
}

package com.example.projection_reflection_test.model.projection;

import org.springframework.data.rest.core.config.Projection;

import com.example.projection_reflection_test.model.Fruit;

@Projection(name = "fruitProjection", types = { Fruit.class })
public interface FruitProjection extends RegisterProjection {

	String getName();
}

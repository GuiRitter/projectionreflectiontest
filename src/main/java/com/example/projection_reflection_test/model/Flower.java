package com.example.projection_reflection_test.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.example.projection_reflection_test.utils.Utils;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name="flower")
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "id_flower")) } )
public class Flower {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@NotBlank
	@Column(name = "name", length = 40)
	protected String name;

	public Flower() {
		id = null;
		name = null;
	}

	public Flower(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Flower(Flower flower) {
		id = flower.id;
		name = flower.name;
	}

	public Flower(String URI) {
		this(Utils.getEntityFromURI(URI, Flower.class));
	}
}

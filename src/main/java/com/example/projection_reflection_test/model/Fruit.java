package com.example.projection_reflection_test.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.example.projection_reflection_test.utils.Utils;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
@Entity
@Table(name="fruit")
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "id_fruit")) } )
public class Fruit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@NotBlank
	@Column(name = "name", length = 40)
	protected String name;

	public Fruit() {
		id = null;
		name = null;
	}

	public Fruit(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Fruit(Fruit fruit) {
		id = fruit.id;
		name = fruit.name;
	}

	public Fruit(String URI) {
		this(Utils.getEntityFromURI(URI, Fruit.class));
	}
}

/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.projection_reflection_test.config;

import com.example.projection_reflection_test.model.Animal;
import com.example.projection_reflection_test.model.Flower;
import com.example.projection_reflection_test.model.Fruit;
import com.example.projection_reflection_test.repository.AnimalRepository;
import com.example.projection_reflection_test.repository.FlowerRepository;
import com.example.projection_reflection_test.repository.FruitRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DatabaseLoader implements CommandLineRunner {

	@Autowired
	private AnimalRepository animalRepository;

	@Autowired
	private FlowerRepository flowerRepository;

	@Autowired
	private FruitRepository fruitRepository;

	@Override
	public void run(String... strings) throws Exception {
		animalRepository.save(new Animal(null, "Dog"));
		animalRepository.save(new Animal(null, "Cat"));
		flowerRepository.save(new Flower(null, "Rose"));
		fruitRepository.save(new Fruit(null, "Apple"));
	}
}

package com.example.projection_reflection_test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import com.example.projection_reflection_test.model.projection.RegisterProjection;

@Component
public abstract class ProjectionResourceProcessor<EntityType, ProjectionType extends RegisterProjection> implements ResourceProcessor<Resource<ProjectionType>> {

    @Autowired
    private EntityLinks entityLinks;

	protected abstract Class<EntityType> getEntityClass();

    @Override
    public Resource<ProjectionType> process(Resource<ProjectionType> resource) {

        resource.add(entityLinks.linkForSingleResource(getEntityClass(), resource.getContent().getId()).withSelfRel());
        return resource;
    }
}

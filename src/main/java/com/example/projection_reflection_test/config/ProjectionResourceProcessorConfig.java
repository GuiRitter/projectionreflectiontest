package com.example.projection_reflection_test.config;

import java.util.List;
import java.util.stream.Collectors;

import com.example.projection_reflection_test.model.Animal;
import com.example.projection_reflection_test.model.Flower;
import com.example.projection_reflection_test.model.Fruit;
import com.example.projection_reflection_test.model.projection.AnimalProjection;
import com.example.projection_reflection_test.model.projection.FlowerProjection;
import com.example.projection_reflection_test.model.projection.FruitProjection;
import com.example.projection_reflection_test.utils.ClassLister;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectionResourceProcessorConfig implements BeanDefinitionRegistryPostProcessor {

	@Bean
	public ProjectionResourceProcessor<Animal, AnimalProjection> animalProjectionResourceProcessor() {
		return new ProjectionResourceProcessor<Animal, AnimalProjection>() {

			@Override
			protected Class<Animal> getEntityClass() {
				return Animal.class;
			}
		};
	}

	@Bean
	public ProjectionResourceProcessor<Fruit, FruitProjection> fruitProjectionResourceProcessor() {
		return new ProjectionResourceProcessor<Fruit, FruitProjection>() {

			@Override
			protected Class<Fruit> getEntityClass() {
				return Fruit.class;
			}
		};
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
	}

	public static class FlowerProcessor extends ProjectionResourceProcessor<Flower, FlowerProjection> {
		@Override
		protected Class<Flower> getEntityClass() {
			return Flower.class;
		}
	}

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		try {
			List<Class<?>> classList = ClassLister.getClassesForPackage("com.example.projection_reflection_test.model")
					.stream()
					.filter(classItem -> !classItem.getName().contains("model.projection"))
					.collect(Collectors.toList());
			System.out.println(classList);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(FlowerProcessor.class);
		registry.registerBeanDefinition("flowerProcessor", rootBeanDefinition);
	}
}

package com.example.projection_reflection_test.service;

import java.net.URI;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.data.geo.format.DistanceFormatter;
import org.springframework.data.geo.format.PointFormatter;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.data.mapping.context.PersistentEntities;
import org.springframework.data.repository.support.DefaultRepositoryInvokerFactory;
import org.springframework.data.repository.support.DomainClassConverter;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.rest.core.UriToEntityConverter;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

@Service
public class EntityConverter {

	@Autowired
	private MappingContext<?, ?> mappingContext;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired(required = false)
	private List<RepositoryRestConfigurer> configurers = Collections.emptyList();

	public <T> T convert(Link link, Class<T> target) {

		DefaultFormattingConversionService conversionService = new DefaultFormattingConversionService();

		Repositories repositories = new Repositories(applicationContext);
		UriToEntityConverter converter = new UriToEntityConverter(
				new PersistentEntities(Collections.singleton(mappingContext)),
				new DefaultRepositoryInvokerFactory(repositories),
				repositories);

		conversionService.addConverter(converter);
		addFormatters(conversionService);
		for (RepositoryRestConfigurer configurer : configurers) {
			configurer.configureConversionService(conversionService);
		}

		URI uri = convert(link);
		T object = target.cast(conversionService.convert(uri, TypeDescriptor.valueOf(target)));
		if (object == null) {
			throw new IllegalArgumentException(String.format("registerNotFound", target.getSimpleName(), uri));
		}
		return object;
	}

	private URI convert(Link link) {
		try {
			return new URI(link.getHref().replace("{?projection}", ""));
		} catch (Exception e) {
			throw new IllegalArgumentException("invalidURI", e);
		}
	}

	private void addFormatters(FormatterRegistry registry) {

		registry.addFormatter(DistanceFormatter.INSTANCE);
		registry.addFormatter(PointFormatter.INSTANCE);

		if (!(registry instanceof FormattingConversionService)) {
			return;
		}

		FormattingConversionService conversionService = (FormattingConversionService) registry;

		DomainClassConverter<FormattingConversionService> converter = new DomainClassConverter<FormattingConversionService>(
				conversionService);
		converter.setApplicationContext(applicationContext);
	}
}

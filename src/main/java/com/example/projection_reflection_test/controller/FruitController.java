package com.example.projection_reflection_test.controller;

import com.example.projection_reflection_test.model.projection.FruitProjection;
import com.example.projection_reflection_test.repository.FruitRepository;
import com.example.projection_reflection_test.utils.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController()
@RequestMapping("api/fruits")
public class FruitController {

	@Autowired
	protected ProjectionFactory factory;

	@Autowired
	private FruitRepository repository;

	@GetMapping()
	public ResponseEntity<Object> get() {

		return ResponseEntity.ok(new Resource<Object>(Utils.iteratorToList(repository.findAll()).stream().map(
			entity -> factory.createProjection(FruitProjection.class, entity))));
	}
}

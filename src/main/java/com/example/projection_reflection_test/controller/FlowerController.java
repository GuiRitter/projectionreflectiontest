package com.example.projection_reflection_test.controller;

import com.example.projection_reflection_test.model.projection.FlowerProjection;
import com.example.projection_reflection_test.repository.FlowerRepository;
import com.example.projection_reflection_test.utils.Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController()
@RequestMapping("api/flowers")
public class FlowerController {

	@Autowired
	protected ProjectionFactory factory;

	@Autowired
	private FlowerRepository repository;

	@GetMapping()
	public ResponseEntity<Object> get() {

		return ResponseEntity.ok(new Resource<Object>(Utils.iteratorToList(repository.findAll()).stream().map(
			entity -> factory.createProjection(FlowerProjection.class, entity))));
	}
}

package com.example.projection_reflection_test;

import java.util.Map;

import com.example.projection_reflection_test.config.Application;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;


@RunWith(SpringRunner.class)
@SpringBootTest(
	classes = Application.class,
	webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private RestTemplate restTemplate;

	@Test
	public void contextLoads() {}

	@Test
	public void haveLinksBeenBuilt() {
		System.out.println(restTemplate.getForObject("http://localhost:" + port + "/api/animals", Map.class));
		System.out.println(restTemplate.getForObject("http://localhost:" + port + "/api/flowers", Map.class));
		System.out.println(restTemplate.getForObject("http://localhost:" + port + "/api/fruits", Map.class));
	}
}
